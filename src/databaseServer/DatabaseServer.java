/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import model.Reservation;

/**
 *
 * @author Alex
 */
public class DatabaseServer{
    
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 
    private static final String DB_URL = "jdbc:mysql://localhost:8889/hotel"; // database address
   
    private static final String USER = "guohua"; //database username
    private static final String PASS = "886868"; // database password
    
    public DatabaseServer (){
        
    }
    private Connection conn = null;
    /*
     * Make the database connection through JDBC, return the connection status.
     */
    public String connectDatabase (){
        try {
            Class.forName(JDBC_DRIVER); 
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            return "Connect Succeed.";
            
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseServer.class.getName()).log(Level.SEVERE, null, ex);
            return "Failed";
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseServer.class.getName()).log(Level.SEVERE, null, ex);
            return "Failed";
        }
    }
    
    /*
     * Get the reservation data in the arraylist
     */
    public ArrayList<Reservation> getReservations() throws SQLException{
        ArrayList<Reservation> reservations = new ArrayList<Reservation>();
        PreparedStatement psd = conn.prepareStatement("select distinct r.reservation_id, cus.customer_name, h.hotel_name, c.city_name, \n" +
        "rt.roomType_name, r.room_id, DATE_FORMAT(r.exp_sdate,'%y-%m-%d'), DATE_FORMAT(r.exp_edate,'%y-%m-%d')\n" +
        "from hotel.reservation r, hotel.customer cus, hotel.cities c, hotel.hotel h, hotel.cities_has_hotel chh, \n" +
        "hotel.room rm, hotel.roomType rt\n" +
        "where r.cus_id = cus.customer_id\n" +
        "and r.room_id = rm.room_id\n" +
        "and rm.roomType_id = rt.roomType_id\n" +
        "and rt.cityHotel_id = chh.cityHotel_id\n" +
        "and chh.cities_city_id = c.city_id\n" +
        "and chh.hotel_id = h.hotel_id\n" +
        "order by h.hotel_name, c.city_name, r.room_id, r.exp_sdate;");
        ResultSet rs = psd.executeQuery();
        while(rs.next()){
            Reservation r = new Reservation(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                    rs.getString(5),rs.getInt(6),rs.getString(7),rs.getString(8));
            reservations.add(r);
        }
        return reservations;
    }
    /*
     * Check room availability by providing the search details
     */
    public ArrayList<Integer> checkAvailability(String city, String hotel, String checkinDate, String checkoutDate, String roomType, int daysOfStay) throws SQLException{
        ArrayList<Integer> result = new ArrayList<Integer>();
        PreparedStatement psd = conn.prepareStatement("select t.room_id\n" +
        "from (\n" +
        "select r.room_id, count(r.room_id) as roomday from hotel.room r, hotel.roomType rt, \n" +
        "hotel.hotel h, hotel.cities c, hotel.cities_has_hotel chh\n" +
        "where r.roomType_id = rt.roomType_id\n" +
        "and rt.cityHotel_id = chh.cityHotel_id\n" +
        "and rt.roomType_name = ?\n" +
        "and chh.cities_city_id = c.city_id\n" +
        "and chh.hotel_id = h.hotel_id\n" +
        "and c.city_name = ?\n" +
        "and h.hotel_name = ?\n" +
        "and r.availability = 1\n" +
        "and r.date between ? and ?\n" +
        "group by r.room_id) t\n" +
        "where roomday = ?;");
        psd.setString(1, roomType);
        psd.setString(2, city);
        psd.setString(3, hotel);
        psd.setString(4, checkinDate);
        psd.setString(5, checkoutDate);
        psd.setInt(6, daysOfStay);
        ResultSet rs = psd.executeQuery();
        
        while(rs.next()) 
        {
            result.add(rs.getInt(1));           
        }
        
        return result; 
    }
    /*
     * Update the room availability by providing the roomId.
     */
    public String updateAvailability(String sdate, String edate, int roomNo, int availability){
        try {
            PreparedStatement psd = conn.prepareStatement("update hotel.room set availability=? where room_id = ? and date between ? and ?;");
            psd.setInt(1, availability);
            psd.setInt(2, roomNo);
            psd.setString(3, sdate);
            psd.setString(4,edate);
            psd.executeUpdate();
            return "update successful";
        } catch (SQLException ex) {
            return "update fail";
        }
    }
    /*
     * Get the room price
     */
    public String getRoomPrice(int roomId) throws SQLException{
        String result = null;
        PreparedStatement psd = conn.prepareStatement("select distinct roomType_rate from hotel.roomType rt, hotel.room r\n" +
        "where rt.roomType_id = r.roomType_id\n" +
        "and r.room_id = ?");
        psd.setInt(1, roomId);
        ResultSet rs = psd.executeQuery();
        while(rs.next()) 
        {
            result =  String.valueOf(rs.getInt(1));           
        }
        return result;
    }
    /*
     * Insert customer to the database
     */
    public int insertCustomer(String name, String contact, String card){
        try {       
            PreparedStatement psd = conn.prepareStatement("insert into hotel.customer(customer_name,customer_contact,customer_creditcard) values(?,?,?);");
            psd.setString(1,name);
            psd.setString(2,contact);
            psd.setString(3,card);  
            psd.executeUpdate();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID();");
            int i =-1;
            if(rs.next()){
                i = rs.getInt(1);
            }
            return i;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseServer.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } 
    }
    /*
     * Insert a room to the database as the dummy data
     */
    public int insertRoom(int roomId, String date, int availability, int roomType){
        try{
            PreparedStatement psd = conn.prepareStatement("insert into hotel.room values(?,?,?,?)");
            psd.setInt(1, roomId);
            psd.setString(2, date);
            psd.setInt(3, availability);
            psd.setInt(4, roomType);
            psd.executeUpdate();
            return 1;
        }
        catch (SQLException ex) {
            Logger.getLogger(DatabaseServer.class.getName()).log(Level.SEVERE, null, ex);
            return 0;  
        }
    }
    
    /*
     * Insert the reservation information to the reservation table
     */
    public int makeReservation(int customer, int room, String checkInDate, String checkOutDate ){
        try {       
            PreparedStatement psd = conn.prepareStatement("insert into hotel.reservation(cus_id,room_id,"
                    + "exp_sdate,exp_edate) values(?,?,?,?);");
            psd.setInt(1,customer);
            psd.setInt(2,room);
            psd.setString(3,checkInDate);
            psd.setString(4, checkOutDate);
            psd.executeUpdate();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID();");
            int i =-1;
            if(rs.next()){
                i = rs.getInt(1);
            }
            return i;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseServer.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    /*
     * Add the dummy room data, 10 rooms for each type of room in a hotel, recorded for one month.
     */
    public String addDummyRoomData (){
        String feedback =null;
        int result =-1;
        int roomId = 34;
        for (int roomType = 5; roomType<19;roomType++){
            for (int j = 0;j<10;j++){
            for (int i = 1;i<32;i++){
                    
                    String date = "2014-07-"+i;
                    result = insertRoom(roomId,date,1,roomType);
                    
                    if(result == 0){
                        feedback = "fail on" + roomType + " " + " " + date + " " +roomId;
                        break;
                    }
                    else{
                        feedback = "Successful";
                    }     
            }
            roomId ++;
            }
            
        }
        return feedback;
    }
    /*
     * Used for test the database connection and functionality.
     */
    public static void main(String[] args) throws SQLException{
        DatabaseServer ds = new DatabaseServer();
        String r = ds.connectDatabase();
        ArrayList<Reservation> rs;
        rs = ds.getReservations();
        System.out.println(rs.get(1).getCusName());
//        String result = ds.addDummyRoomData();
//        ArrayList<Integer> result = new ArrayList<Integer>();
//        result = ds.checkAvailability("Melbourne","Hilton","2014-07-01", "2014-07-03","standard", 3);
//       System.out.println(result);
//        r = ds.updateAvailability("2014-07-02", 1, 1);
//        System.out.println(r);
//        result = ds.checkAvailability("2014-07-01", "2014-07-03", 3, 1);
//        System.out.println(result.size());
        
        //System.out.println(ds.insertCustomer("kkl", "2232", "2322"));
    }
}
