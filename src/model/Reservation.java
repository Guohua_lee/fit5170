/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Alex
 */

public class Reservation implements java.io.Serializable{ // the object send through socket should be sirialized
    private int reservationId;
    private String cusName;
    private String hotelName;
    private String cityName;
    private String roomType;
    private int roomId;
    private String sdate;
    private String edate;

    public Reservation(int reservationId, String cusName, String hotelName, String cityName, String roomType, int roomId, String sdate, String edate) {
        this.reservationId = reservationId;
        this.cusName = cusName;
        this.hotelName = hotelName;
        this.cityName = cityName;
        this.roomType = roomType;
        this.roomId = roomId;
        this.sdate = sdate;
        this.edate = edate;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }
    
}
