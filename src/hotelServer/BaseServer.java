/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelServer;

import databaseServer.DatabaseServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Reservation;

/**
 *
 * @author Alex
 */
public class BaseServer {
    private static DatabaseServer dbServer = new DatabaseServer();
    
    /*
     * Start the server based on the port
     */
    public static void startServer (int port, String serverName){
        ServerSocket s = null;
        try {
            s = new ServerSocket(port);
        } catch(IOException e) {
            System.out.println(e);
            System.exit(1);
        }
        System.out.println("Hotel "+ serverName +" Server Running " + "Port is " + port);
        // Server startup information
        while(true) {
            Socket incomming = null;
            try {
                incomming = s.accept();
            }catch(IOException e) {
                System.out.println(e);
                continue;
            }
            
            new SocketHandler(incomming, dbServer).start();
            
        }
    }
}
    
    
    
    class SocketHandler extends Thread {
    Socket incomming;
    DatabaseServer dbServer;
    public static int currentRoomId=0; // store the roomid after check availability for further reservation
    public static String sdate; //store the checkin data when search for the availability for further reservation
    public static String edate;//store the checkout data when search for the availability for further reservation
    
    
    SocketHandler(Socket incomming, DatabaseServer dbServer) {
        this.incomming = incomming;
        this.dbServer = dbServer;
        
    }
    
    public void run(){
        try {
            BufferedReader reader = 
                    new BufferedReader(new InputStreamReader(
                    incomming.getInputStream()));
            PrintStream out = new PrintStream(incomming.getOutputStream());
            String str = reader.readLine();
            String result = "processing"; // return to the client to tell the current status.
            
            if(str.equals("search")) // the signal from the client, this block is for check the availability
            {
                //get the data from the client server
                String city = reader.readLine();
                String hotel = reader.readLine();
                String checkInDate = reader.readLine();
                checkInDate = "2014-" + checkInDate;
                sdate = checkInDate;
                String checkOutDate = reader.readLine();
                checkOutDate = "2014-" + checkOutDate;
                edate = checkOutDate;
                String roomType = reader.readLine();
                
                int dayOfStay = Integer.parseInt(reader.readLine());
                ArrayList<Integer> resultArray = new ArrayList<Integer>();//store all the available room
                result = dbServer.connectDatabase(); // connected to the database before retieving the data
                
                if(!result.equals("Failed")){
                    try {
                        resultArray = dbServer.checkAvailability(city,hotel,checkInDate, checkOutDate, roomType,dayOfStay);
                        result = String.valueOf(resultArray.size());
                        if(resultArray.size()>0){
                            currentRoomId = resultArray.get(0);// store the first available room 
                        }
                    } catch (SQLException ex) {
                        result = "Failed to get the data";
                    }
                }
                out.println(result); // send the data back to the client
            }
            if(str.equals("booking")) // make reservation
            {
                String name = reader.readLine();
                String contact = reader.readLine();
                String creditNo = reader.readLine();
                int cusId = dbServer.insertCustomer(name, contact, creditNo);
                System.out.println(cusId);
                dbServer.updateAvailability(sdate,edate,currentRoomId,0);
                System.out.println(cusId);
                dbServer.makeReservation(cusId, currentRoomId, sdate, edate);
                System.out.println(cusId);
                result = String.valueOf(currentRoomId);
                String roomPrice;
                try {
                    roomPrice = dbServer.getRoomPrice(currentRoomId);
                } catch (SQLException ex) {
                    Logger.getLogger(hotelServer.SocketHandler.class.getName()).log(Level.SEVERE, null, ex);
                    roomPrice = "failed to get";
                }
                result = result + " The room price is " +roomPrice;
                out.println(result);
            }
            if(str.equals("getReservation")) // get all the reservation data
            {
                //Reservation data in the Arraylist format should be transmitted through socket by objectOutputStream
                ObjectOutputStream objectOutput = new ObjectOutputStream(incomming.getOutputStream());
                ArrayList<Reservation> reservations;// get the reservation from the database
                try {
                    dbServer.connectDatabase();
                    reservations = dbServer.getReservations();
                } catch (SQLException ex) {
                    Logger.getLogger(SocketHandler.class.getName()).log(Level.SEVERE, null, ex);
                    reservations = null;
                }
                objectOutput.writeObject(reservations); // send to client
            }
            
            
            
            incomming.close();
            
        }
        catch(IOException e) {
            e.printStackTrace();
            System.out.println("Failed to accept client");
        }
            
    }
}
