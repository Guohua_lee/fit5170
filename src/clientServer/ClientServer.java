/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Reservation;

/**
 *
 * @author Alex
 */
public class ClientServer {
    public static int PORT = 8189; // the port for the main server
    public static int PORT2 = 8190; // the port for the backup server
    InetAddress address = null;
    Socket socket = null;
    
    public ClientServer(){
        System.out.println("Client Server is Running");
    }
    
    /*
     * Connected to the main server or the backup server, return the connection status.
     */
    public String connectedToServer(){
        
        String response = null; // record the server connected status.
        try {
            address = InetAddress.getByName("localhost");
            
        } catch(UnknownHostException e) {
            e.printStackTrace();
            response = "UnknownHost";
        }
        
        try {
            socket = new Socket(address,PORT); // try to connected to the main server
            response = "Connected main server";
        }
        catch(IOException e) {
            try {
                socket = new Socket(address,PORT2); // if the main server does not response, try connected to the backup server
                response = "Connected backup server";
            } catch (IOException ex) {
                Logger.getLogger(ClientServer.class.getName()).log(Level.SEVERE, null, ex);
                response = "Connection failed";
            }
                  
        }
        
        return response;
    }
    
    /*
     * get the information from the interface, send them to the hotel server to retrieve the data
     * return the number of the room in string.
     */
    public String sendSearchMessage(String city,String hotel,String checkInDate, String checkOutDate, String roomType, int dayOfStay){
        
        String result = null;
        
        InputStream in = null;
        PrintStream out = null;
        
        try {
            in = socket.getInputStream(); // initialized the inputstream
            out = new PrintStream(socket.getOutputStream()); //initialized the outputstream
            out.println("search"); // signal to the server
            out.println(city);
            out.println(hotel);
            out.println(checkInDate);
            out.println(checkOutDate);
            out.println(roomType);
            out.println(dayOfStay);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            try{
                result = reader.readLine(); // get the data or the error message from hotel server
            }
            catch (IOException e) {
                e.printStackTrace();
                result = "Failed to get data from server";
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            result = "Search Failed";
        }   
        return result;
    }
    /*
     * Forward the customer details to the hotel server to make reservation
     */
    public String makeReservation(String name, String contact, String creditNo){
        String result = null;
        
        InputStream in = null;
        PrintStream out = null;
        
        try {
            in = socket.getInputStream();
            out = new PrintStream(socket.getOutputStream());
            out.println("booking");
            out.println(name);
            out.println(contact);
            out.println(creditNo);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            try{
                result = reader.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
                result = "Failed to get data from server";
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            result = "Booking Failed";
        }   
        return result;
    }
    
    /*
     * Get the reservation data from the hotel server return an arraylist.
     */
    public ArrayList<Reservation> checkReservation(){
        ArrayList<Reservation> reservations = new ArrayList<Reservation>();
        
        System.out.println("getresult");
        PrintStream out = null;          
        try {
            out = new PrintStream(socket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(ClientServer.class.getName()).log(Level.SEVERE, null, ex);
        }
            out.println("getReservation");
            ObjectInputStream objectInput = null; // the data from the server is the arraylist, therefore a objectInputstream is needed
            
        try {
            
            objectInput = new ObjectInputStream(socket.getInputStream());
            //the object inputstream should be initialized as long as it can get the data from the socket.
            System.out.println("getr"); 
        } catch (IOException ex) {
            Logger.getLogger(ClientServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("getw");
        }
            try{
                Object object = objectInput.readObject();
                reservations = (ArrayList<Reservation>)object; // Cast the object to the Arraylist
                System.out.println(reservations.get(1).getCityName()); // Test the return data
            }
            catch (IOException e) {
                System.out.println("3");
                e.printStackTrace();
               
            } catch (ClassNotFoundException ex) {
                System.out.println("4");
                Logger.getLogger(ClientServer.class.getName()).log(Level.SEVERE, null, ex);
            } 
        
         
        
        return reservations;
    }
}
